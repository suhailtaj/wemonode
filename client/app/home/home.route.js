(function(){
  'use strict';

  angular
    .module('app.home')
    .config(config);

  function config($stateProvider) {

    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/home/home.html',
        controller: 'HomeCtrl as vm'
      })
      .state('fonts', {
        url: '/fonts',
        templateUrl: 'templates/font.html',
        controller: 'fonts as vm',
      })
      .state('colors', {
        url: '/colors',
        templateUrl: 'templates/color.html',
        controller: 'fonts as vm',
      })
      .state('icons', {
        url: '/icons',
        templateUrl: 'templates/icons.html'
        // controller: 'fonts as vm',
      });
  }
})();