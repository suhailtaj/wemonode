(function() {
    'use strict';

    angular
        .module('app.home')
        .controller('HomeCtrl', HomeCtrl)
        .controller('HeaderCtrl', HeaderCtrl);;

    HomeCtrl.$inject = ['$http', 'constants'];
    HeaderCtrl.$inject = ['$location'];

    function HomeCtrl($http, constants) {

        var self = this;
        self.service = null;
        self.on = on;
        self.off = off;
        self.status = false;
        self.urlConfig = urlConfig;
        self.smsMe = true;
        self.urls = {};
        init();

        function init() {
            $http.get('/api/v1/home').success(function(data) {
                self.service = data
            });

            // Configure URLs
            urlConfig(self.smsMe);

            // Turn OFF the switch
            // off();
        }

        // Configure URL to trrigger
        function urlConfig(flag) {
            if (flag) {
                self.urls.ON = constants.smsMeON;
                self.urls.OFF = constants.smsMeOFF;
            } else {
                self.urls.ON = constants.wemoON;
                self.urls.OFF = constants.wemoOFF;
            }
        }

        function on() {
            self.status = true;
            $http({
                method: 'jsonp',
                url: self.urls.ON,
            }).then(function(response) {

            });
        }

        function off() {
            self.status = false;
            $http({
                method: 'jsonp',
                url: self.urls.OFF,
            }).then(function(response) {

            });
        }
    }

    function HeaderCtrl($location) {
        var vm = this;
        vm.getClass = getClass;

        function getClass(path) {
            if ($location.path().substr(0, path.length) === path) {
                return 'active';
            } else {
                return '';
            }
        }
    }

})();
