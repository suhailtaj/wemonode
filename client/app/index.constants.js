(function() {
    'use strict';

    angular
        .module('app')
        .constant("constants", {
            "smsMeON": "https://maker.ifttt.com/trigger/demoLightOn/with/key/fVkTbApCEkUbtqabiu_mx-dSqT4NP0gsLQGIQso2yGq",
            "smsMeOFF" : "https://maker.ifttt.com/trigger/demoLightOff/with/key/fVkTbApCEkUbtqabiu_mx-dSqT4NP0gsLQGIQso2yGq",
            "wemoON":"https://maker.ifttt.com/trigger/moter_on/with/key/km9BR_rBcaoO2-3pzcIUE9AVoOzpNyVb4Oh6Vw8qiSz",
            "wemoOFF": "https://maker.ifttt.com/trigger/moter_off/with/key/km9BR_rBcaoO2-3pzcIUE9AVoOzpNyVb4Oh6Vw8qiSz"
        });
})();
